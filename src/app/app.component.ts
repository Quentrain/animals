import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'animals';

  selectedAnimal: {
    name: string,
    weight: string,
    speed: string,
    img: string
  } = {
    name: 'Dog',
    weight: '10 - 80 kg',
    speed: '25 - 40 km/h',
    img: '/assets/img/dog.jpg'
  }

  animalsArray: Array<{name: string, weight: string, speed: string, img: string}> = [
    {name: 'Dog', weight: '10 - 80 kg', speed: '25 - 40 km/h', img: '/assets/img/dog.jpg'},
    {name: 'Cat', weight: '4 - 8 kg', speed: '30 - 40 km/h', img: '/assets/img/cat.jpg'},
    {name: 'Horse', weight: '750 – 1 500 kg', speed: '50 – 600 km/h', img: '/assets/img/horse.jpg'},
    {name: 'Rabbit', weight: '3 - 5 kg', speed: '30 - 50 km/h', img: '/assets/img/rabbit.jpg'}
  ];

  selectAnimal(item){
    this.selectedAnimal.name = item.name;
    this.selectedAnimal.weight = item.weight;
    this.selectedAnimal.speed = item.speed;
    this.selectedAnimal.img = item.img;
  }

}
